package ru.chelnokov.myMath;

import java.util.Scanner;

/**
 * Тестовый класс к классу myMath
 *
 * @author Chelnokov E.I., 16IT18K
 */

public class DemoMath {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Добро пожаловать! Что вы хотите вычислить?");
        String task = reader.nextLine();
        String[] partsOfTask = task.split(" ");
        double a = Double.valueOf(partsOfTask[0]);
        double b = Double.valueOf(partsOfTask[2]);
        switch (partsOfTask[1]) {
            case "+":
                System.out.print(task + " = " + MyMath.add(a, b));
                break;
            case "-":
                System.out.print(task + " = " + MyMath.subtract(a, b));
                break;
            case "*":
                System.out.print(task + " = " + MyMath.multiply(a, b));
                break;
            case "/":
                try {
                    System.out.print(task + " = " + MyMath.divide(a, b));
                } catch (ArithmeticException e) {
                    System.out.println("Ай-ай-ай, молодой человек!На ноль делить нельзя.");
                }
                break;
            case "\\":
                try {
                    System.out.print(task + " = " + MyMath.divide((int) a, (int) b));
                } catch (ArithmeticException e) {
                    System.out.println("Ай-ай-ай, молодой человек!На ноль делить нельзя.");
                }
                break;
            case "%":
                System.out.print(task + " = " + MyMath.rest(a, b));
                break;
            case "^":
                System.out.print(task + " = " + MyMath.pow(a, b));
                break;
            default:
                System.out.println("Это за гранью нашего понимания, извините");
        }
    }
}
