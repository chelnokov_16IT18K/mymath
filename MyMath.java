package ru.chelnokov.myMath;

/**
 * <h1>Класс-подобие классу Math, который содержит
 * основные арифметические операции:</h1>
 *
 * <li>Сложение ("+");</li>
 * <li>Вычитание ("-");</li>
 * <li>Умножение ("*");</li>
 * <li>Вещественное деление ("/");</li>
 * <li>Целочисленное деление ("|");</li>
 * <li>Выделение остатка ("%");</li>
 * <li>Возведение в степень ("^").</li>
 */
class MyMath {
    /**
     * Метод для сложения двух чисел
     *
     * @param a первое слагаемое
     * @param b втоорое слагаемое
     * @return сумма a и b
     */

    static double add(double a, double b) {
        return a + b;
    }

    /**
     * Метод для вычитания двух чисел
     *
     * @param a уменьшаемое
     * @param b вычитаемое
     * @return разность a и b
     */
    static double subtract(double a, double b) {
        return a - b;
    }

    /**
     * Метод для умножения двух чисел
     *
     * @param a первый множитель
     * @param b второй множитель
     * @return произведение а и b
     */

    static double multiply(double a, double b) {
        return a * b;
    }

    /**
     * Метод для вещественного деления двух чисел
     *
     * @param a делимое
     * @param b делитель
     * @return частное a и b
     */

    static double divide(double a, double b) {
        if (b == 0){
            throw new ArithmeticException("Ай-ай-ай, молодой человек!На ноль делить нельзя.");
        }
        return a / b;
    }

    /**
     * Метод для целочисленного деления двух чисел
     *
     * @param a делимое
     * @param b делитель
     * @return частное
     */
    static int divide(int a, int b) {
        if (b == 0){
            throw new ArithmeticException("Ай-ай-ай, молодой человек!На ноль делить нельзя.");
        }
        return a / b;
    }

    /**
     * Метод для выведения остатка от деления
     *
     * @param a делимое
     * @param b делитель
     * @return остаток от деления a и b
     */
    static double rest(double a, double b) {
        return a % b;
    }

    /**
     * Метод для возведения в степень числа
     *
     * @param a основание
     * @param degree показатель степени
     * @return c  - число а, возведённое в степень b
     */
    static double pow(double a, double degree) {
        double result = 1;
        double n = 1;
        if(degree < 0 ){
            n = - degree;
        }else{
        n = degree;
        }
        for (int i = 1; i <= n; i++) {
                result = result * a;
        }
        if (degree < 0){
            result = 1 / result;
        }
        return result;
    }
}
